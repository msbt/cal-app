FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

RUN mkdir -p /app/code /app/data
WORKDIR /app/code

# cal.com requires node 14
ARG NODEVERSION=14.19.0
RUN mkdir -p /usr/local/node-${NODEVERSION} && \
     curl -L https://nodejs.org/dist/v${NODEVERSION}/node-v${NODEVERSION}-linux-x64.tar.xz | tar Jxf - --strip-components 1 -C /usr/local/node-${NODEVERSION}

# manually add turbo to path, else it can't launch
ENV PATH /app/code/node_modules/turbo-linux-64/bin:/app/code/node_modules/turbo/bin:/usr/local/node-${NODEVERSION}/bin:$PATH

# cal.com version
ARG VERSION=V1.5.3

# get the thing
RUN curl -L https://github.com/calcom/cal.com/archive/${VERSION}.tar.gz | tar -xz --strip-components 1 -f -

# file and folder setup
RUN ln -s /app/data/env /app/code/.env && \
    ln -s /app/data/env_prisma /app/code/packages/prisma/.env && \
    ln -s /app/data/next /app/code/apps/web/.next && \
    ln -s /app/data/prisma /app/code/packages/prisma/.turbo && \
    rm -rf /home/cloudron/.yarn && ln -s /run/yarn /home/cloudron/.yarn && \
    rm -rf /home/cloudron/.cache && ln -s /run/cache /home/cloudron/.cache && \
    ln -s /app/data/.yarnrc /home/cloudron/.yarnrc && \
    ln -s /run/yarn-error.log /app/code/yarn-error.log && \
    ln -s /app/data/.config /home/cloudron/.config

RUN apt-get update && \
    apt-get -y install netcat && \
    rm -rf /var/lib/apt/lists/* && \
    npm install --global prisma

# install
RUN yarn

# move node_modules around
RUN mv /app/code/node_modules /app/code/node_modules_copy && \
    ln -s /run/calcom/node_modules /app/code/node_modules

RUN chown -R cloudron:cloudron /app/code /app/data /home/cloudron /run

COPY start.sh /app/pkg/

# was required when building on windows, can probably be removed
RUN chmod +x /app/pkg/start.sh

CMD [ "/app/pkg/start.sh" ]
