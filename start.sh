#!/bin/bash

set -eux

echo "=> Creating directories"
mkdir -p /app/data/next /app/data/.config /run/yarn /run/cache/yarn /app/data/cache /run/calcom/node_modules /app/data/prisma

export NODE_PATH=/app/code/node_modules

if [[ ! -f /app/data/env ]]; then
    echo "=> First run"
    cp /app/code/.env.example /app/data/env
    cp /app/code/packages/prisma/.env.example /app/data/env_prisma

    # update environment
    sed -e "s,^BASE_URL=.*,BASE_URL='${CLOUDRON_APP_ORIGIN}'," \
        -e "s,^NEXT_PUBLIC_APP_URL=.*,NEXT_PUBLIC_APP_URL='${CLOUDRON_APP_ORIGIN}'," \
        -e "s,^EMAIL_FROM=.*,EMAIL_FROM='${CLOUDRON_MAIL_FROM}'," \
        -e "s,^EMAIL_SERVER_HOST=.*,EMAIL_SERVER_HOST='${CLOUDRON_MAIL_SMTP_SERVER}'," \
        -e "s,^EMAIL_SERVER_PORT=.*,EMAIL_SERVER_PORT=${CLOUDRON_MAIL_SMTP_PORT}," \
        -e "s,^EMAIL_SERVER_USER=.*,EMAIL_SERVER_USER='${CLOUDRON_MAIL_SMTP_USERNAME}'," \
        -e "s,^EMAIL_SERVER_PASSWORD=.*,EMAIL_SERVER_PASSWORD='${CLOUDRON_MAIL_SMTP_PASSWORD}'," \
        -i /app/data/env

    # update db information
    sed -e "s,^DATABASE_URL=.*,DATABASE_URL='postgresql://${CLOUDRON_POSTGRESQL_USERNAME}:${CLOUDRON_POSTGRESQL_PASSWORD}@${CLOUDRON_POSTGRESQL_HOST}:${CLOUDRON_POSTGRESQL_PORT}/${CLOUDRON_POSTGRESQL_DATABASE}'," \
        -i /app/data/env_prisma

    # set random encryption keys
    export CALENC="$(openssl rand -base64 24)"
    # hex because there was a special char which screwed up the start
    export JWTENC="$(openssl rand -hex 12)"
    export CRONENC="$(openssl rand -base64 24)"

    # telemetry should be opt-in
    sed -e "s,^CALENDSO_ENCRYPTION_KEY=.*,CALENDSO_ENCRYPTION_KEY='${CALENC}'," \
        -e "s,^CRON_API_KEY=.*,CRON_API_KEY='${CRONENC}'," \
        -e "s,^JWT_SECRET=.*,JWT_SECRET='${JWTENC}'," \
        -e "s,^NEXT_PUBLIC_TELEMETRY_KEY=.*,#NEXT_PUBLIC_TELEMETRY_KEY=''," \
    -i /app/data/env

    cd /app/code
    #gosu cloudron:cloudron yarn workspace @calcom/prisma db-deploy --schema /app/code/packages/prisma/schema.prisma

fi

echo "=> Copy node_modules"
cp -rf /app/code/node_modules_copy/* /run/calcom/node_modules

chown -R cloudron:cloudron /app/data /run/

echo "=> Updating environment"
sed -e "s,^DATABASE_URL=.*,DATABASE_URL='postgresql://${CLOUDRON_POSTGRESQL_USERNAME}:${CLOUDRON_POSTGRESQL_PASSWORD}@${CLOUDRON_POSTGRESQL_HOST}:${CLOUDRON_POSTGRESQL_PORT}/${CLOUDRON_POSTGRESQL_DATABASE}'," \
    -e "s,^BASE_URL=.*,BASE_URL='${CLOUDRON_APP_ORIGIN}'," \
    -e "s,^NEXT_PUBLIC_APP_URL=.*,NEXT_PUBLIC_APP_URL='${CLOUDRON_APP_ORIGIN}'," \
    -e "s,^EMAIL_FROM=.*,EMAIL_FROM='${CLOUDRON_MAIL_FROM}'," \
    -e "s,^EMAIL_SERVER_HOST=.*,EMAIL_SERVER_HOST='${CLOUDRON_MAIL_SMTP_SERVER}'," \
    -e "s,^EMAIL_SERVER_PORT=.*,EMAIL_SERVER_PORT=${CLOUDRON_MAIL_SMTP_PORT}," \
    -e "s,^EMAIL_SERVER_USER=.*,EMAIL_SERVER_USER='${CLOUDRON_MAIL_SMTP_USERNAME}'," \
    -e "s,^EMAIL_SERVER_PASSWORD=.*,EMAIL_SERVER_PASSWORD='${CLOUDRON_MAIL_SMTP_PASSWORD}'," \
    -i /app/data/env

echo "=> Updating db information"
sed -e "s,^DATABASE_URL=.*,DATABASE_URL='postgresql://${CLOUDRON_POSTGRESQL_USERNAME}:${CLOUDRON_POSTGRESQL_PASSWORD}@${CLOUDRON_POSTGRESQL_HOST}:${CLOUDRON_POSTGRESQL_PORT}/${CLOUDRON_POSTGRESQL_DATABASE}'," \
    -i /app/data/env_prisma


echo "=> Build it"
gosu cloudron:cloudron yarn build && yarn install --production --ignore-scripts --prefer-offline


echo "=> Run migrations in case of update"
cd /app/code
gosu cloudron:cloudron npx prisma migrate deploy --schema /app/code/packages/prisma/schema.prisma


if [[ ! -f /app/data/db ]]; then
echo "=> Create initial db-user"
    # hacky solution because of $-signs in the password hash, adds a user with admin@cloudron.local:changeme
    PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "INSERT INTO users (id, username, email, password, plan, metadata) VALUES (1, 'cloudron', 'admin@cloudron.local', '\$2a\$12\$wMDZD4IcrCbY3JUEgkVeS.UxSgkgMY2RTVNpGU6h1wRuI7SYQgBd2', 'PRO', '{}');"
    touch /app/data/db
fi


echo "==> Starting Cal.com"
exec /usr/local/bin/gosu cloudron:cloudron yarn start
